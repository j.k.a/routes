// Ortografía y estilos correctos
import React, { useEffect, useState } from "react";
import { useTheme } from "../ThemeContext";
import {
  Image,
  TouchableOpacity,
  View,
  SafeAreaView,
  Modal,
  Button,
  TextInput,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { StyleSheet, Text } from "react-native";
import { FontFamily, Color, FontSize, Padding } from "../GlobalStyles";
import axios from "axios";
import { DataTable } from "react-native-paper";

const Perfil = ({ route }) => {
  const { isDarkMode } = useTheme();
  const [modalVisible, setModalVisible] = useState(false);
  const [nuevaContrasena, setNuevaContrasena] = useState("");
  const [confirmarContrasena, setConfirmarContrasena] = useState("");
  const [mostrarContrasenaNueva, setMostrarContrasenaNueva] = useState(false);
  const [mostrarContrasenaConfirmar, setMostrarContrasenaConfirmar] =
    useState(false);

  const darkModeArrow = require("../assets/icleftBlanc.png");
  const lightModeArrow = require("../assets/icleft.png");

  const imageSource = isDarkMode ? darkModeArrow : lightModeArrow;
  const [mensajeError, setMensajeError] = useState("");
  const navigation = useNavigation();

  const irAtras = () => {
    navigation.goBack();
  };

  const openModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
    setMensajeError("");
    setNuevaContrasena("");
    setConfirmarContrasena("");
    setMostrarContrasenaNueva(false);
    setMostrarContrasenaConfirmar(false);
  };

  const cambiarContrasena = () => {
    if (nuevaContrasena === confirmarContrasena) {
      setMensajeError("Contraseña cambiada correctamente");
    } else {
      setMensajeError("Las contraseñas no coinciden");
    }
  };

  const { empleado } = route.params;

  const [datosEmpleado, setDatosEmpleado] = useState(null);

  useEffect(() => {
    const obtenerDatosEmpleado = async () => {
      try {
        const response = await axios.get(
          `https://polliwog-desired-egret.ngrok-free.app/tasks/api/v1/empleados/${empleado}/`
        );
        setDatosEmpleado(response.data);
      } catch (error) {
        console.error("Error al obtener los datos del empleado:", error);
      }
    };

    obtenerDatosEmpleado();
  }, [empleado]);

  return (
    <SafeAreaView style={isDarkMode ? styles.darkBackground : styles.safeArea}>
      <View style={styles.perfil}>
        <View style={styles.topBar}>
          <View style={[styles.content, styles.buttonFlexBox]}>
            <TouchableOpacity onPress={irAtras}>
              <Image
                style={styles.icLeftIcon}
                contentFit="cover"
                source={imageSource}
              />
            </TouchableOpacity>
            <Text
              style={[
                styles.title,
                styles.titleTypo,
                isDarkMode ? styles.darkText : styles.lightText,
              ]}
            >
              Perfil
            </Text>
          </View>
        </View>
        <View style={[styles.sectionTitle1, styles.buttonFlexBox]}>
          <View style={styles.text}>
            <Text
              style={[
                styles.title1,
                styles.titleTypo,
                isDarkMode ? styles.darkText : styles.lightText,
              ]}
            >
              Información del empleado:
            </Text>
          </View>
        </View>
        <View style={[styles.list, styles.listFlexBox]}>
          <View style={[styles.item, styles.listFlexBox]}>
            {datosEmpleado && (
              <DataTable>
                <DataTable.Row>
                  <DataTable.Cell>
                    <Text
                      style={isDarkMode ? styles.darkText : styles.lightText}
                    >
                      Nombre
                    </Text>
                  </DataTable.Cell>
                  <DataTable.Cell>
                    <Text
                      style={isDarkMode ? styles.darkText : styles.lightText}
                    >
                      {datosEmpleado.nombrepila}
                    </Text>
                  </DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                  <DataTable.Cell>
                    <Text
                      style={isDarkMode ? styles.darkText : styles.lightText}
                    >
                      Apellido paterno
                    </Text>
                  </DataTable.Cell>
                  <DataTable.Cell>
                    <Text
                      style={isDarkMode ? styles.darkText : styles.lightText}
                    >
                      {datosEmpleado.primerapellido}
                    </Text>
                  </DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                  <DataTable.Cell>
                    <Text
                      style={isDarkMode ? styles.darkText : styles.lightText}
                    >
                      Apellido materno
                    </Text>
                  </DataTable.Cell>
                  <DataTable.Cell>
                    <Text
                      style={isDarkMode ? styles.darkText : styles.lightText}
                    >
                      {datosEmpleado.segundoapellido}
                    </Text>
                  </DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                  <DataTable.Cell>
                    <Text
                      style={isDarkMode ? styles.darkText : styles.lightText}
                    >
                      Calle
                    </Text>
                  </DataTable.Cell>

                  <DataTable.Cell>
                    <Text
                      style={isDarkMode ? styles.darkText : styles.lightText}
                    >
                      {datosEmpleado.calle}
                    </Text>
                  </DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                  <DataTable.Cell>
                    <Text
                      style={isDarkMode ? styles.darkText : styles.lightText}
                    >
                      No. Exterior
                    </Text>
                  </DataTable.Cell>
                  <DataTable.Cell>
                    <Text
                      style={isDarkMode ? styles.darkText : styles.lightText}
                    >
                      {datosEmpleado.numext}
                    </Text>
                  </DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                  <DataTable.Cell>
                    <Text
                      style={isDarkMode ? styles.darkText : styles.lightText}
                    >
                      Colonia
                    </Text>
                  </DataTable.Cell>
                  <DataTable.Cell>
                    <Text
                      style={isDarkMode ? styles.darkText : styles.lightText}
                    >
                      {datosEmpleado.colonia}
                    </Text>
                  </DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                  <DataTable.Cell>
                    <Text
                      style={isDarkMode ? styles.darkText : styles.lightText}
                    >
                      Teléfono
                    </Text>
                  </DataTable.Cell>
                  <DataTable.Cell>
                    <Text
                      style={isDarkMode ? styles.darkText : styles.lightText}
                    >
                      {datosEmpleado.numtel}
                    </Text>
                  </DataTable.Cell>
                </DataTable.Row>
              </DataTable>
            )}
          </View>
        </View>

        {/* Modal */}
        <Modal transparent={true} visible={modalVisible} animationType="slide">
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "rgba(0, 0, 0, 0.5)",
            }}
          >
            <View
              style={{
                backgroundColor: "white",
                padding: 20,
                borderRadius: 10,
              }}
            >
              <Text style={{ fontSize: 20, marginBottom: 20 }}>
                Cambiar Contraseña
              </Text>
              <View style={styles.inputContainer}>
                <TextInput
                  placeholder="Nueva Contraseña"
                  secureTextEntry={!mostrarContrasenaNueva}
                  value={nuevaContrasena}
                  onChangeText={(text) => setNuevaContrasena(text)}
                />
                <TouchableOpacity
                  style={styles.showPasswordButton}
                  onPress={() =>
                    setMostrarContrasenaNueva(!mostrarContrasenaNueva)
                  }
                >
                  <Text>{mostrarContrasenaNueva ? "Ocultar" : "Mostrar"}</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.inputContainer}>
                <TextInput
                  placeholder="Confirmar Contraseña"
                  style={styles.input}
                  secureTextEntry={!mostrarContrasenaConfirmar}
                  value={confirmarContrasena}
                  onChangeText={(text) => setConfirmarContrasena(text)}
                />
                <TouchableOpacity
                  onPress={() =>
                    setMostrarContrasenaConfirmar(!mostrarContrasenaConfirmar)
                  }
                >
                  <Text>
                    {mostrarContrasenaConfirmar ? "Ocultar" : "Mostrar"}
                  </Text>
                </TouchableOpacity>
              </View>
              {mensajeError !== "" && <Text>{mensajeError}</Text>}
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 20,
                }}
              >
                <Button
                  color="#000"
                  title="Guardar"
                  onPress={cambiarContrasena}
                />
                <Button color="#000" title="Cerrar" onPress={closeModal} />
              </View>
            </View>
          </View>
        </Modal>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  topIconLayout: {
    overflow: "hidden",
    maxWidth: "100%",
  },
  darkBackground: {
    flex: 1,
    backgroundColor: Color.colorDiscord,
  },
  darkText: {
    color: "#fff",
  },
  lightText: {
    color: "#000",
  },
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  buttonFlexBox: {
    alignItems: "center",
    flexDirection: "row",
  },
  borde: {
    borderWidth: 1,
    borderColor: "#000",
  },
  titleTypo: {
    textAlign: "left",
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 24,
    color: Color.colorBlack,
  },
  listFlexBox: {
    justifyContent: "left",
    alignSelf: "stretch",
  },
  title3Typo: {
    fontFamily: FontFamily.robotoRegular,
    lineHeight: 16,
    fontSize: FontSize.size_xs,
    textAlign: "left",
  },
  topIcon: {
    height: 24,
    alignSelf: "stretch",
    width: "100%",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    flex: 1,
  },
  content: {
    paddingLeft: Padding.p_base,
    paddingTop: Padding.p_xs,
    paddingRight: Padding.p_5xs,
    alignSelf: "stretch",
    paddingBottom: Padding.p_xs,
  },
  contentT: {
    paddingTop: 8,
    paddingRight: Padding.p_5xs,
    paddingBottom: Padding.p_xs,
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center",
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
  },
  title1: {
    fontSize: FontSize.size_lg,
    alignSelf: "stretch",
  },
  text: {
    flex: 1,
  },
  sectionTitle: {
    paddingBottom: 8,
    paddingTop: Padding.p_base,
    paddingHorizontal: Padding.p_xs,
    alignSelf: "stretch",
  },
  sectionTitle1: {
    paddingTop: 8,
    paddingHorizontal: Padding.p_xs,
    alignSelf: "stretch",
  },
  subtitle: {
    color: Color.colorGray_500,
    alignSelf: "stretch",
  },
  subtitleWrapper: {
    zIndex: 0,
    flex: 1,
  },
  itemChild: {
    position: "absolute",
    right: 0,
    bottom: -1,
    left: 0,
    maxHeight: "100%",
    zIndex: 1,
  },
  item: {
    paddingHorizontal: 0,
    paddingVertical: Padding.p_xs,
    flexDirection: "row",
    justifyContent: "center",
  },
  list: {
    paddingVertical: 0,
    paddingHorizontal: Padding.p_xs,
  },
  title3: {
    color: Color.colorBlack,
    lineHeight: 16,
    fontSize: FontSize.size_xs,
  },
  icon: {
    width: 12,
    height: 12,
    marginLeft: 2,
  },
  button: {
    height: "auto",
    width: "auto",
    borderRadius: 4,
    borderStyle: "solid",
    borderColor: Color.colorBlack,
    borderWidth: 1,
    paddingLeft: Padding.p_5xs,
    paddingTop: Padding.p_10xs,
    paddingRight: Padding.p_9xs,
    paddingBottom: Padding.p_10xs,
  },
  perfil: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
  },
});
export default Perfil;
