import React, { useState } from "react";
import axios from "axios"; // Import axios
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
  TextInput,
  Modal,
  Button, // Importar Button
} from "react-native";
import { FontSize, FontFamily, Color, Padding, Border } from "../GlobalStyles";
import { useNavigation } from "@react-navigation/native";
import { useTheme } from "../ThemeContext";

const InicioDeSesion = () => {
  const [mostrarContrasenaNueva, setMostrarContrasenaNueva] = useState(false);
  const [empleado, setEmpleado] = useState("");
  const [contra, setContra] = useState("");
  const [modalVisible, setModalVisible] = useState(false);
  const [mensajeError, setMensajeError] = useState("");
  const navigation = useNavigation();
  const { isDarkMode } = useTheme();

  // Define las rutas de las imágenes
  const darkModeImage = require("../assets/image-61Blanc.png");
  const lightModeImage = require("../assets/image-61.png");

  // Determina qué imagen usar según el modo oscuro
  const imageSource = isDarkMode ? darkModeImage : lightModeImage;

  const limpiarDatosSesion = () => {
    setEmpleado("");
    setContra("");
  };

  const irAPrincipal = () => {
    limpiarDatosSesion(); // Limpiar los datos antes de navegar a la página principal
    setModalVisible(false);
    navigation.navigate("Principal", { empleado: empleado });
  };

  const irAMenu = () => {
    navigation.navigate("Menu");
  };

  const irASoporte = () => {
    navigation.navigate("SoporteTecnico");
  };

  const handleEmpleadoChange = (text) => {
    setEmpleado(text.replace(/[^0-9]/g, ""));
  };

  const Inicio = async () => {
    try {
      const response = await axios.get(
        // `https://rare-early-rabbit.ngrok-free.app/tasks/api/v1/empleados/${empleado}/` //Gera
        // `https://polliwog-desired-egret.ngrok-free.app/tasks/api/v1/empleados/${empleado}/` //Jordan
        `https://precisely-logical-bunny.ngrok-free.app/tasks/api/v1/empleados/${empleado}/`
      );
      if (response.data.contrasena === contra) {
        setMensajeError("Inicio de sesión exitoso");
      } else {
        setMensajeError("Contraseña incorrecta");
      }
    } catch (error) {
      setMensajeError("Número de empleado incorrecto");
    } finally {
      setModalVisible(true);
    }
  };

  const closeModal = () => {
    setModalVisible(false);
    setMensajeError("");
  };

  return (
    <SafeAreaView style={isDarkMode ? styles.darkBackground : styles.safeArea}>
      <View style={styles.sesion}>
        <View style={styles.container}>
          <Text
            style={[
              styles.title,
              isDarkMode ? styles.darkText : styles.lightText,
            ]}
          >
            Inicio de sesión
          </Text>

          <View style={[styles.input, styles.inputSpaceBlock]}>
            <Text
              style={[
                styles.title2,
                styles.text1Typo,
                isDarkMode ? styles.darkText : styles.lightText,
              ]}
            >
              Número de empleado
            </Text>
            <View style={[styles.textfield, styles.textfieldBorder]}>
              <TextInput
                style={[
                  styles.text1,
                  styles.text1Typo,
                  isDarkMode ? styles.darkText : styles.lightText,
                ]}
                value={empleado}
                onChangeText={handleEmpleadoChange}
                placeholder="Número de empleado"
                placeholderTextColor={
                  isDarkMode ? styles.darkText.color : styles.lightText.color
                }
                keyboardType="numeric"
              />
            </View>
          </View>
          <View style={[styles.input, styles.inputSpaceBlock]}>
            <Text
              style={[
                styles.title2,
                styles.text1Typo,
                isDarkMode ? styles.darkText : styles.lightText,
              ]}
            >
              Ingrese su contraseña
            </Text>
            <View style={[styles.textfield, styles.textfieldBorder]}>
              <TextInput
                style={[
                  styles.text1,
                  styles.text1Typo,
                  isDarkMode ? styles.darkText : styles.lightText,
                ]}
                secureTextEntry={!mostrarContrasenaNueva}
                value={contra}
                onChangeText={setContra}
                placeholder="Contraseña"
                placeholderTextColor={
                  isDarkMode ? styles.darkText.color : styles.lightText.color
                }
                keyboardType="default"
              />
              <TouchableOpacity
                style={styles.showPasswordButton}
                onPress={() =>
                  setMostrarContrasenaNueva(!mostrarContrasenaNueva)
                }
              >
                <Text style={isDarkMode ? styles.darkText : styles.lightText}>
                  {mostrarContrasenaNueva ? "Ocultar" : "Mostrar"}
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          {/* Modal */}
          <Modal
            transparent={true}
            visible={modalVisible}
            animationType="slide"
          >
            <View style={styles.modalContainer}>
              <View style={styles.modalContent}>
                {mensajeError !== "" && (
                  <Text style={styles.errorText}>{mensajeError}</Text>
                )}
                <View style={styles.buttonContainer12}>
                  <Button
                    color="#000"
                    title="Aceptar"
                    onPress={() => {
                      if (mensajeError === "Inicio de sesión exitoso") {
                        irAPrincipal();
                      } else {
                        closeModal();
                      }
                    }}
                  />
                </View>
              </View>
            </View>
          </Modal>

          <View style={styles.buttonContainer}>
            <TouchableOpacity onPress={Inicio} style={styles.button1}>
              <Text style={styles.buttonText1}>Iniciar Sesión</Text>
            </TouchableOpacity>
          </View>

          <TouchableOpacity onPress={irASoporte} style={styles.supportButton}>
            <View style={styles.bottomNav}>
              <View style={styles.tab}>
                <Image
                  style={styles.image6Icon}
                  contentFit="cover"
                  source={imageSource}
                />
                <Text
                  style={[
                    styles.title3,
                    isDarkMode ? styles.darkText : styles.lightText,
                  ]}
                  numberOfLines={1}
                >
                  Soporte Técnico
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  darkBackground: {
    flex: 1,
    backgroundColor: Color.colorDiscord,
  },
  darkText: {
    color: "#fff", // Texto blanco para modo oscuro
  },
  lightText: {
    color: "#000", // Texto negro para modo claro
  },
  text1: {
    fontFamily: FontFamily.robotoRegular,
    height: 20,
    overflow: "hidden",
    flex: 1,
  },
  modalContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  modalContent: {
    backgroundColor: "white",
    padding: 20,
    borderRadius: 10,
  },
  modalTitle: {
    fontSize: 20,
    marginBottom: 20,
    textAlign: "center",
  },
  textfieldBorder: {
    marginTop: 4,
    borderWidth: 1,
    borderColor: Color.colorGray_200,
    borderStyle: "solid",
    borderRadius: Border.br_7xs,
    alignSelf: "stretch",
    alignItems: "center",
  },
  textfield: {
    paddingVertical: Padding.p_5xs,
    paddingHorizontal: Padding.p_xs,
    flexDirection: "row",
  },
  text1Typo: {
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    textAlign: "left",
  },
  inputSpaceBlock: {
    paddingVertical: 0,
    paddingHorizontal: Padding.p_xs,
    overflow: "hidden",
    alignSelf: "stretch",
  },
  sesion: {
    flex: 1,
    paddingBottom: Padding.p_xs,
  },
  title2: {
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    fontSize: FontSize.size_sm,
    alignSelf: "stretch",
  },
  safeArea: {
    flex: 1,
  },
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: Padding.p_xs,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
  },
  input: {
    marginBottom: 20,
  },
  label: {
    fontSize: 16,
    marginBottom: 5,
  },
  textField: {
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 5,
    padding: 10,
  },
  placeholder: {
    color: "#999",
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 20,
  },
  buttonContainer12: {
    paddingTop: 14,
    flexDirection: "row",
    justifyContent: "center",
  },
  button1: {
    backgroundColor: "#000",
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#000",
  },
  button2: {
    backgroundColor: "#fff",
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#000",
  },
  buttonText1: {
    color: "#fff",
    fontSize: 16,
    textAlign: "center",
  },
  buttonText2: {
    color: "#000",
    fontSize: 16,
    textAlign: "center",
  },
  supportButton: {
    width: "auto",
    height: "auto",
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: "center",
  },
  supportButtonText: {
    color: "#fff",
    fontSize: 16,
    textAlign: "center",
  },
  bottomNav: {
    width: "auto",
    height: "auto",
    marginTop: 12,
    overflow: "hidden",
  },
  tab: {
    padding: Padding.p_9xs,
    height: 53,
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  image6Icon: {
    width: 29,
    height: 29,
  },
  title3: {
    fontSize: FontSize.size_3xs,
    lineHeight: 14,
    textAlign: "center",
    display: "flex",
    height: 14,
    justifyContent: "center",
    alignSelf: "stretch",
    alignItems: "center",
  },
});

export default InicioDeSesion;
