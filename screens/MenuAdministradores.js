import * as React from "react";
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  SafeAreaView,
  Image,
  ScrollView,
  Modal,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import ConfiguracionContainer from "../components/ConfiguracionContainer";
import { FontSize, FontFamily, Color, Padding, Border } from "../GlobalStyles";

const MenuAdministradores = () => {
  const navigation = useNavigation();
  const [modalVisible, setModalVisible] = React.useState(false);

  const irASoporte = () => {
    navigation.navigate("SoporteTecnico");
  };
  const irAAjustes = () => {
    navigation.navigate("Ajustes");
  };
  const irAPerfil = () => {
    navigation.navigate("Perfil");
  };
  const irARutas = () => {
    navigation.navigate("Rutas");
  };
  const irAtras = () => {
    navigation.goBack();
  };
  const irRegistroEmpleados = () => {
    navigation.navigate("RegistroDeEmpleado");
  };
  const irRegistroConductor = () => {
    navigation.navigate("RegistroDeConductor");
  };
  const irRegistroTransporte = () => {
    navigation.navigate("RegistroDeTransporte");
  };
  const irCrearRuta = () => {
    navigation.navigate("CreacionDeRutas");
  };

  const closeModal = () => {
    setModalVisible(false);
  };

  const openModal = () => {
    setModalVisible(true);
  };

  const irAInicioSesion = () => {
    navigation.navigate("InicioDeSesion");
    closeModal();
  };

  return (
    <ScrollView style={styles.safeArea}>
      <View style={styles.menuAdministradores}>
        <View style={styles.topBar}>
          <View style={[styles.content, styles.rowFlexBox]}>
            <TouchableOpacity onPress={irAtras}>
              <Image
                style={styles.icLeftIcon}
                contentFit="cover"
                source={require("../assets/icleft.png")}
              />
            </TouchableOpacity>
            <Text style={[styles.title, styles.titleTypo]}>Menu</Text>
          </View>
        </View>
        <View style={[styles.listSpaceBlock]}>
          <TouchableOpacity onPress={irAAjustes}>
            <ConfiguracionContainer
              iconEmoji="⚙️"
              menuOptionText="Configuración"
              propAlignSelf="stretch"
              propWidth="unset"
              propColor="#000"
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={irARutas}>
            <ConfiguracionContainer
              iconEmoji="🔍"
              menuOptionText="Ver rutas"
              propAlignSelf="stretch"
              propWidth="unset"
              propColor="#000"
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={irAPerfil}>
            <ConfiguracionContainer
              iconEmoji="👤"
              menuOptionText="Perfil"
              propAlignSelf="stretch"
              propWidth="unset"
              propColor="#000"
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={irRegistroEmpleados}>
            <ConfiguracionContainer
              iconEmoji="👨‍💼"
              menuOptionText="Registrar empleado"
              propAlignSelf="stretch"
              propWidth="unset"
              propColor="#000"
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={irRegistroConductor}>
            <ConfiguracionContainer
              iconEmoji="🚖"
              menuOptionText="Registrar conductor"
              propAlignSelf="stretch"
              propWidth="unset"
              propColor="#000"
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={irRegistroTransporte}>
            <ConfiguracionContainer
              iconEmoji="🚚"
              menuOptionText="Registrar transporte"
              propAlignSelf="stretch"
              propWidth="unset"
              propColor="#000"
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={irCrearRuta}>
            <ConfiguracionContainer
              iconEmoji="🗺️"
              menuOptionText="Crear ruta"
              propAlignSelf="stretch"
              propWidth="unset"
              propColor="#000"
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={irASoporte}>
            <ConfiguracionContainer
              iconEmoji="🗝️"
              menuOptionText="Soporte Tecnico "
              propAlignSelf="unset"
              propWidth={336}
              propColor="#000"
            />
          </TouchableOpacity>
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              setModalVisible(!modalVisible);
            }}
          >
            <View style={styles.modalOverlay}>
              <View style={styles.modalContent}>
                <View style={styles.container}>
                  <View
                    style={[
                      styles.confirmacionCierreDeSesion,
                      styles.cardFlexBox,
                    ]}
                  >
                    <View style={[styles.card, styles.cardBorder]}>
                      <Text style={styles.titleMod}>
                        ¿Está seguro que desea cerrar su sesión?
                      </Text>
                      <View style={styles.buttonContainer}>
                        <TouchableOpacity onPress={closeModal}>
                          <View style={[styles.button, styles.secondary]}>
                            <Text style={[styles.title2, styles.titleTypoMod]}>
                              Cancelar
                            </Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.buttonContainer}>
                        <TouchableOpacity onPress={irAInicioSesion}>
                          <View style={[styles.button, styles.secondary1]}>
                            <Text style={[styles.title1, styles.titleTypo]}>
                              Cerrar sesión
                            </Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </Modal>
          <TouchableOpacity onPress={openModal}>
            <ConfiguracionContainer
              iconEmoji="🔒"
              menuOptionText="Cerrar sesión "
              propAlignSelf="unset"
              propWidth={336}
              propColor="#000"
            />
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  titleTypo: {
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 24,
    textAlign: "left",
  },
  titleTypoMod: {
    fontFamily: "Roboto",
    fontWeight: "bold",
  },
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  rowFlexBox: {
    flexDirection: "row",
    alignSelf: "stretch",
  },
  title2: {
    color: "#fff",
    fontSize: 16,
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title1: {
    color: "#000",
    fontSize: 16,
  },
  secondary: {
    backgroundColor: "#000",
  },
  secondary1: {
    backgroundColor: "#fff",
  },
  buttonContainer: {
    paddingTop: 14,
    flexDirection: "row",
    justifyContent: "space-around",
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    alignItems: "center",
  },
  button: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#000",
  },
  listSpaceBlock: {
    marginTop: 12,
    alignSelf: "stretch",
  },
  title: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    textAlign: "left",
    flex: 1,
  },
  titleMod: {
    fontSize: FontSize.size_lg,
    lineHeight: 24,
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
    color: Color.colorBlack,
    textAlign: "left",
    alignSelf: "stretch",
  },
  text: {
    flex: 1,
  },
  sectionTitle: {
    flexDirection: "row",
    paddingTop: Padding.p_base,
    paddingHorizontal: Padding.p_xs,
    marginTop: 12,
    alignItems: "center",
  },
  list: {
    justifyContent: "center",
    paddingVertical: 0,
    paddingHorizontal: Padding.p_xs,
    marginTop: 12,
    alignItems: "center",
  },
  image: {
    borderRadius: Border.br_7xs,
    backgroundColor: Color.colorGray_300,
    flex: 1,
  },
  menuAdministradores: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
    backgroundColor: Color.colorWhite,
  },
  modalOverlay: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    justifyContent: "center",
    alignItems: "center",
  },
  modalContent: {
    backgroundColor: "white",
    borderRadius: 10,
    padding: 20,
    elevation: 5,
  },
});

export default MenuAdministradores;
