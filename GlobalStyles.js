/* fonts */
export const FontFamily = {
  robotoRegular: "Roboto-Regular",
  robotoMedium: "Roboto-Medium",
};
/* font sizes */
export const FontSize = {
  size_3xs: 10,
  size_sm: 14,
  size_xs: 12,
  size_xl: 20,
  size_lg: 18,
  size_base: 16,
};
/* Colors */
export const Color = {
  colorWhite: "#fff",
  colorBlack: "#000",
  colorDiscord: "#201c24",
  colorGray_100: "rgba(0, 0, 0, 0.3)",
  colorGray_200: "rgba(0, 0, 0, 0.1)",
  colorGray_300: "rgba(255, 255, 255, 0.05)",
  colorGray_400: "rgba(0, 0, 0, 0.05)",
  colorGray_500: "rgba(0, 0, 0, 0.5)",
  colorRed: "#cf0000",
};
/* Paddings */
export const Padding = {
  p_9xs: 4,
  p_xs: 12,
  p_base: 16,
  p_5xs: 8,
  p_3xs: 10,
  p_10xs: 3,
};
/* border radiuses */
export const Border = {
  br_base: 16,
  br_7xs: 6,
  br_21xl: 40,
  br_5xs: 8,
  br_81xl: 100,
};
