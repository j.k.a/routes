import * as React from "react";
import { Image } from "expo-image";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";
import { useNavigation } from "@react-navigation/native";

import ScheduleContainer from "../components/ScheduleContainer";
import HomeBottomNavContainer from "../components/HomeBottomNavContainer";
import { Color, FontFamily, FontSize, Padding, Border } from "../GlobalStyles";
import { useTheme } from "../ThemeContext";

const ZonaCentroNocturno = () => {
  const navigation = useNavigation();
  const { isDarkMode } = useTheme();

  const irAtras = () => {
    navigation.goBack();
  };
  const darkModeArrow = require("../assets/icleftBlanc.png");
  const lightModeArrow = require("../assets/icleft.png");

  const imageSource = isDarkMode ? darkModeArrow : lightModeArrow;

  return (
    <SafeAreaView style={isDarkMode ? styles.darkBackground : styles.safeArea}>
      <View style={styles.zonaCentroNocturno}>
        <View style={styles.topBar}>
          <View style={[styles.content, styles.contentFlexBox]}>
            <TouchableOpacity onPress={irAtras}>
              <Image
                style={styles.icLeftIcon}
                contentFit="cover"
                source={imageSource}
              />
            </TouchableOpacity>
            <Text
              style={[
                isDarkMode ? styles.darkText : styles.lightText,
                styles.title,
                styles.titleTypo,
              ]}
            >
              Horarios de parada
            </Text>
          </View>
        </View>
        <View style={[styles.sectionTitle, styles.imageSpaceBlock]}>
          <View style={styles.text}>
            <Text
              style={[
                isDarkMode ? styles.darkText : styles.lightText,
                styles.title1,
                styles.titleTypo,
              ]}
            >
              Zona Centro (Nocturno)
            </Text>
          </View>
        </View>
        <ScheduleContainer
          estimatedTime="Hora estimada: 9:30 PM"
          estimatedTimeLabel="Hora estimada: 9:40 PM"
          estimatedTimeDisplay="Hora estimada: 9:55 PM"
          estimatedTimeFormatted="Hora estimada: 10:05 PM"
          estimatedTimeText="Hora estimada: 10:20 PM"
          estimatedTimeDescription="Hora estimada: 10:35 PM"
          estimatedTimeLabelText="Hora estimada: 10:50 PM"
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  darkBackground: {
    flex: 1,
    backgroundColor: Color.colorDiscord,
  },
  darkText: {
    color: "#fff", // Texto blanco para modo oscuro
  },
  lightText: {
    color: "#000", // Texto negro para modo claro
  },

  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  contentFlexBox: {
    flexDirection: "row",
    alignItems: "center",
  },
  titleTypo: {
    textAlign: "left",
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 24,
  },
  imageSpaceBlock: {
    marginTop: 12,
    alignSelf: "stretch",
  },
  topIcon: {
    maxWidth: "100%",
    overflow: "hidden",
    height: 24,
    alignSelf: "stretch",
    width: "100%",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    flex: 1,
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    alignSelf: "stretch",
  },
  topBar: {
    alignSelf: "stretch",
  },
  title1: {
    fontSize: FontSize.size_lg,
    alignSelf: "stretch",
  },
  text: {
    flex: 1,
  },
  sectionTitle: {
    paddingHorizontal: Padding.p_xs,
    paddingTop: Padding.p_base,
    flexDirection: "row",
    alignItems: "center",
  },
  image: {
    borderRadius: Border.br_7xs,
    backgroundColor: Color.colorGray_300,
    height: 164,
  },
  zonaCentroNocturno: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
});

export default ZonaCentroNocturno;
