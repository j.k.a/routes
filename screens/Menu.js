import * as React from "react";
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Modal,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { FontSize, FontFamily, Color, Padding, Border } from "../GlobalStyles";
import { useTheme } from "../ThemeContext";

const MenuAdministradores = () => {
  const navigation = useNavigation();
  const { isDarkMode } = useTheme();
  const [modalVisible, setModalVisible] = React.useState(false);
  const darkModeArrow = require("../assets/icleftBlanc.png");
  const lightModeArrow = require("../assets/icleft.png");

  const imageSource = isDarkMode ? darkModeArrow : lightModeArrow;
  const irASoporte = () => {
    navigation.navigate("SoporteTecnico");
  };
  const irAAjustes = () => {
    navigation.navigate("Ajustes");
  };
  const irAPerfil = () => {
    navigation.navigate("Perfil");
  };
  const irARutas = () => {
    navigation.navigate("Rutas");
  };
  const irAtras = () => {
    navigation.goBack();
  };

  const closeModal = () => {
    setModalVisible(false);
  };

  const openModal = () => {
    setModalVisible(true);
  };

  const irAInicioSesion = () => {
    navigation.navigate("InicioDeSesion");
    closeModal();
  };

  return (
    <ScrollView style={isDarkMode ? styles.darkBackground : styles.safeArea}>
      <View style={styles.menuAdministradores}>
        <View style={styles.topBar}>
          <View style={[styles.content, styles.rowFlexBox]}>
            <TouchableOpacity onPress={irAtras}>
              <Image
                style={styles.icLeftIcon}
                contentFit="cover"
                source={imageSource}
              />
            </TouchableOpacity>
            <Text
              style={[
                styles.titleMenu,
                styles.titleTypo,
                isDarkMode ? styles.darkText : styles.lightText,
              ]}
            >
              Menú
            </Text>
          </View>
        </View>
        <View style={styles.listSpaceBlock}>
          {/* Configuración */}
          <TouchableOpacity onPress={irAAjustes}>
            <View
              style={[
                styles.item,
                styles.itemFlexBox,
                { alignSelf: "stretch" },
              ]}
            >
              <View style={[styles.frame, styles.iconLayout]}>
                <Text
                  style={[styles.icon, styles.iconPosition]}
                  numberOfLines={1}
                >
                  ⚙️
                </Text>
              </View>
              <View style={styles.titleWrapper}>
                <Text
                  style={[
                    styles.title,
                    isDarkMode ? styles.darkText : styles.lightText,
                  ]}
                >
                  Configuración
                </Text>
              </View>
              <Image
                style={[styles.itemChild, styles.iconPosition]}
                contentFit="cover"
                source={require("../assets/vector-2001.png")}
              />
            </View>
          </TouchableOpacity>

          {/* Ver rutas */}
          <TouchableOpacity onPress={irARutas}>
            <View
              style={[
                styles.item,
                styles.itemFlexBox,
                { alignSelf: "stretch" },
              ]}
            >
              <View style={[styles.frame, styles.iconLayout]}>
                <Text
                  style={[styles.icon, styles.iconPosition]}
                  numberOfLines={1}
                >
                  🔍
                </Text>
              </View>
              <View style={styles.titleWrapper}>
                <Text
                  style={[
                    styles.title,
                    isDarkMode ? styles.darkText : styles.lightText,
                  ]}
                >
                  Ver rutas
                </Text>
              </View>
              <Image
                style={[styles.itemChild, styles.iconPosition]}
                contentFit="cover"
                source={require("../assets/vector-2001.png")}
              />
            </View>
          </TouchableOpacity>

          {/* Soporte técnico */}
          <TouchableOpacity onPress={irASoporte}>
            <View
              style={[
                styles.item,
                styles.itemFlexBox,
                { alignSelf: "stretch" },
              ]}
            >
              <View style={[styles.frame, styles.iconLayout]}>
                <Text
                  style={[styles.icon, styles.iconPosition]}
                  numberOfLines={1}
                >
                  🗝️
                </Text>
              </View>
              <View style={styles.titleWrapper}>
                <Text
                  style={[
                    styles.title,
                    isDarkMode ? styles.darkText : styles.lightText,
                  ]}
                >
                  Soporte Técnico
                </Text>
              </View>
              <Image
                style={[styles.itemChild, styles.iconPosition]}
                contentFit="cover"
                source={require("../assets/vector-2001.png")}
              />
            </View>
          </TouchableOpacity>

          {/* Modal */}
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              setModalVisible(!modalVisible);
            }}
          >
            <View
              style={[
                styles.modalOverlay,
                {
                  backgroundColor: isDarkMode
                    ? "rgba(255, 255, 255, 0.5)"
                    : "rgba(0, 0, 0, 0.5)",
                },
              ]}
            >
              <View
                style={isDarkMode ? styles.darkBackground : styles.modalContent}
              >
                <Text style={styles.titleMod}>
                  ¿Está seguro que desea cerrar su sesión?
                </Text>
                <View style={styles.buttonContainer}>
                  <TouchableOpacity onPress={closeModal}>
                    <View style={[styles.button, styles.secondary]}>
                      <Text style={[styles.title2, styles.titleTypoMod]}>
                        Cancelar
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={styles.buttonContainer}>
                  <TouchableOpacity onPress={irAInicioSesion}>
                    <View style={[styles.button, styles.secondary1]}>
                      <Text style={[styles.title1, styles.titleTypo]}>
                        Cerrar sesión
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
          {/* Cerrar sesión */}
          <TouchableOpacity onPress={openModal}>
            <View
              style={[
                styles.item,
                styles.itemFlexBox,
                { alignSelf: "stretch" },
              ]}
            >
              <View style={[styles.frame, styles.iconLayout]}>
                <Text
                  style={[styles.icon, styles.iconPosition]}
                  numberOfLines={1}
                >
                  🔒
                </Text>
              </View>
              <View style={styles.titleWrapper}>
                <Text
                  style={[
                    styles.title,
                    isDarkMode ? styles.darkText : styles.lightText,
                  ]}
                >
                  Cerrar sesión
                </Text>
              </View>
              <Image
                style={[styles.itemChild, styles.iconPosition]}
                contentFit="cover"
                source={require("../assets/vector-2001.png")}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

// Estilos para el componente integrado
const styles = StyleSheet.create({
  darkText: {
    color: "#fff", // Texto blanco para modo oscuro
  },
  lightText: {
    color: "#000", // Texto negro para modo claro
  },
  titleTypo: {
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 24,
    textAlign: "left",
  },
  titleTypoMod: {
    fontFamily: "Roboto",
    fontWeight: "bold",
  },
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  darkBackground: {
    backgroundColor: Color.colorDiscord,
  },
  rowFlexBox: {
    flexDirection: "row",
    alignSelf: "stretch",
  },
  title2: {
    color: "#fff",
    fontSize: 16,
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title1: {
    color: "#000",
    fontSize: 16,
  },
  secondary: {
    backgroundColor: "#000",
  },
  secondary1: {
    backgroundColor: "#fff",
  },
  buttonContainer: {
    paddingTop: 14,
    flexDirection: "row",
    justifyContent: "space-around",
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    alignItems: "center",
  },
  button: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#000",
  },
  listSpaceBlock: {
    marginTop: 12,
    alignSelf: "stretch",
  },
  titleMenu: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    textAlign: "left",
    flex: 1,
  },
  titleMod: {
    fontSize: FontSize.size_lg,
    lineHeight: 24,
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
    color: Color.colorBlack,
    textAlign: "left",
    alignSelf: "stretch",
  },
  text: {
    flex: 1,
  },
  sectionTitle: {
    flexDirection: "row",
    paddingTop: Padding.p_base,
    paddingHorizontal: Padding.p_xs,
    marginTop: 12,
    alignItems: "center",
  },
  list: {
    justifyContent: "center",
    paddingVertical: 0,
    paddingHorizontal: Padding.p_xs,
    marginTop: 12,
    alignItems: "center",
  },
  image: {
    borderRadius: Border.br_7xs,
    backgroundColor: Color.colorGray_300,
    flex: 1,
  },
  menuAdministradores: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
  },
  modalOverlay: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    justifyContent: "center",
    alignItems: "center",
  },
  modalContent: {
    backgroundColor: "white",
    borderRadius: 10,
    padding: 20,
    elevation: 5,
  },
  // Estilos del componente integrado
  itemFlexBox: {
    justifyContent: "center",
    alignItems: "center",
  },
  iconLayout: {
    height: 32,
    width: 32,
  },
  iconPosition: {
    overflow: "hidden",
    position: "absolute",
  },
  iconClr: {
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoRegular,
  },
  icon: {
    marginTop: -16,
    marginLeft: -16,
    top: "50%",
    left: "50%",
    fontSize: FontSize.size_xl,
    lineHeight: 32,
    textAlign: "center",
    display: "flex",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoRegular,
    height: 32,
    width: 32,
    justifyContent: "center",
    alignItems: "center",
  },
  frame: {
    borderRadius: Border.br_base,
    backgroundColor: Color.colorGray_400,
    zIndex: 0,
  },
  title: {
    fontSize: FontSize.size_sm,
    lineHeight: 20,
    textAlign: "left",
    alignSelf: "stretch",
  },
  titleWrapper: {
    flex: 1,
    zIndex: 1,
    marginLeft: 8,
  },
  itemChild: {
    right: 0,
    bottom: -1,
    left: 0,
    maxWidth: "100%",
    maxHeight: "100%",
    zIndex: 2,
  },
  item: {
    flexDirection: "row",
    paddingHorizontal: 0,
    paddingVertical: Padding.p_xs,
    alignSelf: "stretch",
  },
});

export default MenuAdministradores;
