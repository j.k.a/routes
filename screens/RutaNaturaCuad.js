import React, { Component } from "react";
import { Dimensions, StyleSheet, View } from "react-native";
import MapView, { Marker } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";

const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE = 32.38938212719607;
const LONGITUDE = -116.93595124504029;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const GOOGLE_MAPS_APIKEY = "AIzaSyBuAjMS_UH8pCAJKcpxTwkbzg6iItU4xMc";
class RutaNatura extends Component {
  constructor(props) {
    super(props);

    this.state = {
      coordinates: [
        { latitude: 32.38938212719607, longitude: -116.93595124504029 },
        { latitude: 32.410887955718685, longitude: -116.92079017502483 },
        { latitude: 32.40896414813957, longitude: -116.9462168647684 },
        { latitude: 32.41032672689166, longitude: -116.96274619705193 },
        { latitude: 32.420427962336255, longitude: -116.96975864120346 },
        { latitude: 32.45399998188011, longitude: -116.88099210803543 },
      ],
      stopNames: [
        "Inicio, Blvd Las Delicias Sur",
        "Hacienda Los Venados",
        "OXXO Delicias",
        "Metalera, frente al Parque Villas del Prado 1",
        "Caseta Policial, junto al Parque Villas del Prado",
        "Haemonetics",
      ],
    };

    this.mapView = null;
  }

  onReady = (result) => {
    this.mapView.fitToCoordinates(result.coordinates, {
      edgePadding: {
        right: width / 10,
        bottom: height / 10,
        left: width / 10,
        top: height / 10,
      },
    });
  };

  onError = (errorMessage) => {
    console.log(errorMessage);
  };

  render() {
    return (
      <View style={styles.mapContainer}>
        <MapView
          initialRegion={{
            latitude: LATITUDE,
            longitude: LONGITUDE,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }}
          style={styles.map}
          ref={(c) => (this.mapView = c)}
        >
          {this.state.coordinates.map((coordinate, index) => (
            <Marker
              key={index}
              coordinate={coordinate}
              title={this.state.stopNames[index]}
            />
          ))}
          <MapViewDirections
            origin={this.state.coordinates[0]}
            destination={
              this.state.coordinates[this.state.coordinates.length - 1]
            }
            waypoints={this.state.coordinates.slice(1, -1)}
            mode="DRIVING"
            apikey={GOOGLE_MAPS_APIKEY}
            language="en"
            strokeWidth={4}
            strokeColor="#5065F6"
            onStart={(params) => {
              console.log(
                `Started routing between "${params.origin}" and "${
                  params.destination
                }"${
                  params.waypoints.length
                    ? " using waypoints: " + params.waypoints.join(", ")
                    : ""
                }`
              );
            }}
            onReady={this.onReady}
            onError={this.onError}
            resetOnChange={false}
          />
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mapContainer: {
    width: 350, // Define el ancho del contenedor del mapa
    height: 400, // Define la altura del contenedor del mapa
    borderRadius: 10, // Opcional: agrega bordes redondeados para mejorar la apariencia
    overflow: "hidden", // Opcional: asegúrate de que el mapa no se desborde del contenedor
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default RutaNatura;
