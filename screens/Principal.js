import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
} from "react-native";

import { useNavigation } from "@react-navigation/native";
import { Color, FontFamily, FontSize, Padding, Border } from "../GlobalStyles";
import RutaCentroCuad from "./RutaCentroCuad"; // Asegúrate de importar correctamente el archivo que contiene el componente RutaCentro
import RutaVillaFontanaCuad from "./RutaVillaFontanaCuad"; // Asegúrate de importar correctamente el archivo que contiene el componente RutaCentro
import RutaNaturaCuad from "./RutaNaturaCuad"; // Asegúrate de importar correctamente el archivo que contiene el componente RutaCentro
import { useTheme } from "../ThemeContext";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { DataTable } from "react-native-paper";

const Principal = ({ route }) => {
  const navigation = useNavigation();
  const { isDarkMode } = useTheme();

  const darkModeImage = require("../assets/image-6Blanc.png");
  const lightModeImage = require("../assets/image-6.png");

  const imageSource = isDarkMode ? darkModeImage : lightModeImage;

  const irAMenu = () => {
    navigation.navigate("Menu");
  };

  const irAPerfil = () => {
    navigation.navigate("Perfil", { empleado: empleado });
  };
  const { empleado } = route.params;
  const [datosEmpleado, setDatosEmpleado] = useState(null);

  const backgroundStyle = isDarkMode
    ? styles.darkBackground
    : styles.lightBackground;

  useEffect(() => {
    const obtenerDatosEmpleado = async () => {
      try {
        const response = await axios.get(
          `https://precisely-logical-bunny.ngrok-free.app/tasks/api/v1/empleados/${empleado}/`
        );
        setDatosEmpleado(response.data);
      } catch (error) {
        console.error("Error al obtener los datos del empleado:", error);
      }
    };

    obtenerDatosEmpleado();
  }, [empleado]);

  const [empleadoTransporte, setEmpleadoTransporte] = useState(null);
  const [marca, setMarca] = useState(null);
  const [modelo, setModelo] = useState(null);

  useEffect(() => {
    const fetchEmpleadoTransporteDetalle = async () => {
      try {
        const transporteResponse = await axios.get(
          `https://precisely-logical-bunny.ngrok-free.app/tasks/api/v1/empleadostransp/${empleado}/`
        );
        const transporteEmpleado = transporteResponse.data.transporte;

        const transporteDetalleResponse = await axios.get(
          `https://precisely-logical-bunny.ngrok-free.app/tasks/api/v1/transportes/${transporteEmpleado}/`
        );
        const empleadoTransporte = transporteDetalleResponse.data;

        const marcaResponse = await axios.get(
          `https://precisely-logical-bunny.ngrok-free.app/tasks/api/v1/marca/${empleadoTransporte.marca}/`
        );
        const marca = marcaResponse.data;

        const modeloResponse = await axios.get(
          `https://precisely-logical-bunny.ngrok-free.app/tasks/api/v1/modelo/${empleadoTransporte.modelo}/`
        );
        const modelo = modeloResponse.data;

        setEmpleadoTransporte(empleadoTransporte);
        setMarca(marca);
        setModelo(modelo);
      } catch (error) {
        console.error("Error al obtener los detalles del transporte:", error);
      }
    };

    fetchEmpleadoTransporteDetalle();
  }, [empleado]);

  return (
    <SafeAreaView style={isDarkMode ? styles.darkBackground : styles.safeArea}>
      <View style={[styles.principal, backgroundStyle]}>
        <View style={[styles.topBar, styles.topBarShadowBox]}>
          <View style={styles.content}>
            <Text
              style={[
                styles.title,
                styles.titleTypo,
                isDarkMode ? styles.darkText : styles.lightText,
              ]}
            >
              Gestión del transporte:
            </Text>
          </View>
        </View>
        <View>
          {datosEmpleado && (
            <View style={styles.contentT}>
              <TouchableOpacity onPress={irAPerfil}>
                <Text
                  style={[
                    styles.empleadoPerfil,
                    isDarkMode ? styles.darkText : styles.lightText,
                  ]}
                >
                  {datosEmpleado.nombrepila} {datosEmpleado.primerapellido}
                </Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
        <View style={styles.cent}>
          <RutaCentroCuad style={styles.rutaCentro}></RutaCentroCuad>
        </View>
        <View style={[styles.sectionTitle, styles.bottomNavSpaceBlock]}>
          <View style={styles.text}>
            <Text
              style={[
                styles.title1,
                styles.titleTypo,
                isDarkMode ? styles.darkText : styles.lightText,
              ]}
            >
              Información de transporte:
            </Text>
          </View>
        </View>
        <View style={{ alignItems: "flex-start" }}>
          {empleadoTransporte && (
            <DataTable>
              <DataTable.Row>
                <DataTable.Cell>
                  <Text style={isDarkMode ? styles.darkText : styles.lightText}>
                    No. Transporte
                  </Text>
                </DataTable.Cell>
                <DataTable.Cell>
                  <Text style={isDarkMode ? styles.darkText : styles.lightText}>
                    {empleadoTransporte.notransporte}
                  </Text>
                </DataTable.Cell>
              </DataTable.Row>

              <DataTable.Row>
                <DataTable.Cell>
                  <Text style={isDarkMode ? styles.darkText : styles.lightText}>
                    Matrícula
                  </Text>
                </DataTable.Cell>
                <DataTable.Cell>
                  <Text style={isDarkMode ? styles.darkText : styles.lightText}>
                    {empleadoTransporte.matricula}
                  </Text>
                </DataTable.Cell>
              </DataTable.Row>
            </DataTable>
          )}

          {marca && (
            <DataTable>
              <DataTable.Row>
                <DataTable.Cell>
                  <Text style={isDarkMode ? styles.darkText : styles.lightText}>
                    Marca
                  </Text>
                </DataTable.Cell>
                <DataTable.Cell>
                  <Text style={isDarkMode ? styles.darkText : styles.lightText}>
                    {marca.nombre}
                  </Text>
                </DataTable.Cell>
              </DataTable.Row>
            </DataTable>
          )}

          {modelo && (
            <DataTable>
              <DataTable.Row>
                <DataTable.Cell>
                  <Text style={isDarkMode ? styles.darkText : styles.lightText}>
                    Modelo
                  </Text>
                </DataTable.Cell>
                <DataTable.Cell>
                  <Text style={isDarkMode ? styles.darkText : styles.lightText}>
                    {modelo.nombre}
                  </Text>
                </DataTable.Cell>
              </DataTable.Row>
            </DataTable>
          )}
        </View>
        <TouchableOpacity onPress={irAMenu} style={styles.bottomNavButton}>
          <View style={styles.bottomNav}>
            <View style={styles.tab}>
              <Image
                style={styles.image6Icon}
                contentFit="cover"
                source={imageSource}
              />
              <Text
                style={[
                  styles.title3,
                  isDarkMode ? styles.darkText : styles.lightText,
                ]}
                numberOfLines={1}
              >
                Menú
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  cent: {
    alignItems: "center",
  },
  izq: {
    alignContent: "left",
  },
  empleado: {
    fontSize: FontSize.size_sm,
    lineHeight: 20,
    marginLeft: 12,
    alignSelf: "stretch",
  },
  empleadoPerfil: {
    fontSize: FontSize.size_lg,
    lineHeight: 20,
    marginLeft: 16,
    alignSelf: "stretch",
    fontWeight: "500",
  },
  rutaCentro: {
    marginTop: 10, // Opcional: Agrega un margen superior para separar la RutaCentro de otros elementos
  },
  darkBackground: {
    flex: 1,
    backgroundColor: Color.colorDiscord,
  },
  darkText: {
    color: "#fff",
  },
  lightText: {
    color: "#000",
  },
  title3: {
    fontSize: FontSize.size_xs,
    lineHeight: 14,
    textAlign: "center",
    display: "flex",
    height: 14,
    justifyContent: "center",
    color: Color.colorBlack,
    alignSelf: "stretch",
    alignItems: "center",
  },
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  image: {
    borderRadius: Border.br_7xs,
    backgroundColor: Color.colorGray_300,
    flex: 1,
  },
  listSpaceBlock: {
    marginTop: 12,
    alignSelf: "stretch",
  },
  principal: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
  },
  topBarShadowBox: {
    shadowOpacity: 1,
    elevation: 6,
    shadowRadius: 6,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowColor: "rgba(0, 0, 0, 0.12)",
  },
  titleTypo: {
    textAlign: "left",
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 24,
  },
  bottomNavSpaceBlock: {
    marginTop: 12,
    flexDirection: "row",
  },
  tabFlexBox: {
    justifyContent: "center",
    alignItems: "center",
  },
  topIcon: {
    maxWidth: "100%",
    height: 24,
    overflow: "hidden",
    alignSelf: "stretch",
    width: "100%",
  },
  title: {
    fontSize: FontSize.size_xl,
    flex: 1,
  },
  content: {
    paddingLeft: Padding.p_base,
    paddingTop: Padding.p_xs,
    paddingRight: Padding.p_5xs,
    paddingBottom: Padding.p_xs,
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center",
  },
  contentT: {
    paddingRight: Padding.p_5xs,
    paddingBottom: Padding.p_xs,
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center",
  },
  topBar: {
    alignSelf: "stretch",
  },
  title1: {
    fontSize: FontSize.size_lg,
    alignSelf: "stretch",
  },
  text: {
    flex: 1,
  },
  sectionTitle: {
    paddingHorizontal: Padding.p_xs,
    paddingTop: Padding.p_base,
    alignSelf: "stretch",
    alignItems: "center",
  },
  image6Icon: {
    width: 24,
    height: 24,
  },
  title2: {
    fontSize: FontSize.size_3xs,
    lineHeight: 14,
    fontFamily: FontFamily.robotoRegular,
    color: Color.colorBlack,
    textAlign: "center",
    justifyContent: "center",
    overflow: "hidden",
    alignSelf: "stretch",
  },
  tab: {
    padding: Padding.p_9xs,
    height: 53,
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  bottomNavButton: {
    width: "auto",
    height: "auto",
    position: "absolute",
    bottom: 12,
    left: 0,
    right: 0,
    alignItems: "center",
  },
  bottomNav: {
    width: "auto",
    height: "auto",
    marginTop: 12,
    overflow: "visible",
  },
});

export default Principal;
