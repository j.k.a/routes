/*O*/
import * as React from "react";
import {
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { useTheme } from "../ThemeContext";
import { Padding, FontFamily, Color, Border, FontSize } from "../GlobalStyles";
import { ThemeProvider } from "../ThemeContext";

const Ajustes = () => {
  const { isDarkMode } = useTheme();
  const navigation = useNavigation();
  const irAProx = () => {
    navigation.navigate("Proximamente");
  };
  const irASoporte = () => {
    navigation.navigate("SoporteTecnico");
  };
  const irAtras = () => {
    navigation.goBack();
  };
  const irAcerca = () => {
    navigation.navigate("AcercaDeNosotros");
  };
  const irModoOscuro = () => {
    navigation.navigate("ModoOscuro");
  };
  const darkModeArrow = require("../assets/icleftBlanc.png");
  const lightModeArrow = require("../assets/icleft.png");

  const imageSource = isDarkMode ? darkModeArrow : lightModeArrow;

  return (
    <ThemeProvider>
      <ScrollView style={isDarkMode ? styles.darkBackground : styles.safeArea}>
        <View style={styles.Ajustes}>
          <View style={styles.topBar}>
            <View style={[styles.content, styles.rowFlexBox]}>
              <TouchableOpacity onPress={irAtras}>
                <Image
                  style={styles.icLeftIcon}
                  contentFit="cover"
                  source={imageSource}
                />
              </TouchableOpacity>
              <Text
                style={[
                  styles.title,
                  styles.titleTypo,
                  isDarkMode ? styles.darkText : styles.lightText,
                ]}
              >
                Configuración
              </Text>
            </View>
          </View>

          <View style={styles.list}>
            <TouchableOpacity onPress={irModoOscuro} style={styles.item}>
              <View style={styles.iconContainer}>
                <Text style={styles.icon}>🌑</Text>
              </View>
              <View style={styles.textContainer}>
                <Text
                  style={[
                    isDarkMode ? styles.darkText : styles.lightText,
                    styles.featureLabel,
                  ]}
                >
                  Modo Oscuro
                </Text>
                <Text
                  style={[
                    styles.description,
                    isDarkMode ? styles.darkText : styles.lightText,
                  ]}
                >
                  Cambiar a modo oscuro
                </Text>
              </View>
              <Image
                style={styles.itemChild}
                source={require("../assets/vector-2001.png")}
              />
            </TouchableOpacity>

            <TouchableOpacity onPress={irAProx} style={styles.item}>
              <View style={styles.iconContainer}>
                <Text style={styles.icon}>🔔</Text>
              </View>
              <View style={styles.textContainer}>
                <Text
                  style={[
                    isDarkMode ? styles.darkText : styles.lightText,
                    styles.featureLabel,
                  ]}
                >
                  Notificaciones
                </Text>
                <Text
                  style={[
                    styles.description,
                    isDarkMode ? styles.darkText : styles.lightText,
                  ]}
                >
                  {" "}
                  Administra la configuración de notificaciones
                </Text>
              </View>
              <Image
                style={styles.itemChild}
                source={require("../assets/vector-2001.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={irAProx} style={styles.item}>
              <View style={styles.iconContainer}>
                <Text style={styles.icon}>🌐</Text>
              </View>
              <View style={styles.textContainer}>
                <Text
                  style={[
                    isDarkMode ? styles.darkText : styles.lightText,
                    styles.featureLabel,
                  ]}
                >
                  Idioma
                </Text>
                <Text
                  style={[
                    styles.description,
                    isDarkMode ? styles.darkText : styles.lightText,
                  ]}
                >
                  Cambiar a inglés
                </Text>
              </View>
              <Image
                style={styles.itemChild}
                source={require("../assets/vector-2001.png")}
              />
            </TouchableOpacity>
          </View>
          {/* <View style={styles.sectionTitle}>
          <Text style={styles.title4}>Cuenta</Text>
        </View>
        <TouchableOpacity onPress={handleImagePickerPress}>
          <ProfilePhotoUploaderContainer
            iconOrText="📷"
            profileImage={profileImage} // Pasar la imagen de perfil como prop
            profileImageText="Foto de perfil"
            profileActionText="Cambiar o subir una foto de perfil"
          />
        </TouchableOpacity> */}

          <View style={styles.sectionTitle}>
            <Text
              style={[
                styles.title4,
                isDarkMode ? styles.darkText : styles.lightText,
              ]}
            >
              Acerca de
            </Text>
          </View>
          <View style={styles.list}>
            <TouchableOpacity onPress={irAcerca}>
              <View style={styles.item}>
                <View style={styles.iconContainer}>
                  <Text style={styles.icon}>📔</Text>
                </View>
                <View style={styles.textContainer}>
                  <Text
                    style={[
                      isDarkMode ? styles.darkText : styles.lightText,
                      styles.featureLabel,
                    ]}
                  >
                    Acerca de nosotros
                  </Text>
                  <Text
                    style={[
                      styles.description,
                      isDarkMode ? styles.darkText : styles.lightText,
                    ]}
                  >
                    {" "}
                    Conozca más sobre nuestra compañía
                  </Text>
                </View>
                <Image
                  style={styles.itemChild}
                  source={require("../assets/vector-2001.png")}
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </ThemeProvider>
  );
};

const styles = StyleSheet.create({
  description: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.robotoRegular,
  },
  featureLabel: {
    fontSize: FontSize.size_sm,
    fontFamily: FontFamily.robotoRegular,
  },
  iconContainer: {
    width: 32,
    height: 32,
    borderRadius: Border.br_base,
    justifyContent: "center",
    alignItems: "center",
  },
  textContainer: {
    flex: 1,
    marginLeft: 8,
  },
  icon: {
    fontSize: FontSize.size_xl,
    lineHeight: 32,
    textAlign: "center",
    fontFamily: FontFamily.robotoRegular,
  },
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  darkBackground: {
    flex: 1,
    backgroundColor: Color.colorDiscord,
  },
  darkText: {
    color: "#fff", // Texto blanco para modo oscuro
  },
  lightText: {
    color: "#000", // Texto negro para modo claro
  },
  titleTypo: {
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 24,
    textAlign: "left",
  },
  container: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
  },
  rowFlexBox: {
    flexDirection: "row",
    alignSelf: "stretch",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    textAlign: "left",
    flex: 1,
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    alignItems: "center",
  },
  tabGroup: {
    flexDirection: "row",
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
  },
  tabBorder: {
    flex: 1,
    padding: Padding.p_5xs,
    borderWidth: 1,
    borderColor: Color.colorGray_200,
    borderRadius: Border.br_7xs,
    alignItems: "center",
  },
  tab1: {
    marginLeft: 8,
  },
  Ajustes: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
  title1: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.robotoRegular,
    color: Color.colorBlack,
  },
  sectionTitle: {
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
  },
  title4: {
    fontSize: FontSize.size_lg,
    fontFamily: FontFamily.robotoMedium,
    color: Color.colorBlack,
  },
  list: {
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
  },
  frameIcon: {
    width: 32,
    height: 32,
    borderRadius: Border.br_base,
  },
  item: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 0,
    paddingVertical: Padding.p_xs,
  },
  titleParent: {
    flex: 1,
    marginLeft: 8,
  },
  title7: {
    fontSize: FontSize.size_sm,
    fontFamily: FontFamily.robotoRegular,
    color: Color.colorBlack,
  },
  subtitle: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.robotoRegular,
    color: Color.colorGray_500,
  },
  itemChild: {
    position: "absolute",
    right: 0,
    bottom: -1,
    left: 0,
    zIndex: 2,
  },
});

export default Ajustes;
